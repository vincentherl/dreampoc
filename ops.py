#https://github.com/theskumar/python-dotenv#installation

from dotenv import load_dotenv,find_dotenv

load_dotenv(override=True,dotenv_path=find_dotenv())

from os import getenv

key = getenv("api-affise-key",'')
uri = getenv("api-affise-uri",'')
env = getenv("env",'test')
pg_dsn = getenv("pg-dsn")

def isEnvTest() -> bool:
    return env == "test"