### Install

Install python3 and pip
```
sudo apt-get install python3 python3-pip
```

Install virtualenv
```
pip install -U virtualenv
virtualenv ./venv -p python3
source ./dreampoc/bin/activate
python -m pip install -r ./requirements.txt
```

