import sys
from api import ApiAffiseStats
from model import save
from schedule_day import ScheduleDate
from datetime import timedelta,datetime,date

format = "%d-%m-%Y"

start_day = date.today()
end_day = date.today()

if len(sys.argv) == 3:
    start_day=sys.argv[1]
    end_day=sys.argv[2]

    start_day = datetime.strptime(start_day,format).date()
    end_day = datetime.strptime(end_day,format).date()

elif len(sys.argv) == 2:
    start_day = sys.argv[1]
    start_day = datetime.strptime(start_day,format).date()


print("start day : ", start_day)
print("end day : ", end_day)

print("## offer ##")

for day in ScheduleDate.schedule_by_day(start_day,end_day + timedelta(days=1)):
    d = ApiAffiseStats(day, day + timedelta(days=1),10000)
    print('start import %s ... ' % (day), end='', flush=True)
    for data in d.api_get_offer_walk():
        save(data)
        print('x', end='', flush=True)
    print('\n', end='', flush=True)