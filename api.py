import requests, json, ops
from datetime import date
from model import ModelAffiseStats

class ApiAffise():

    def __init__(self):
        self.key = ops.key
        self.uri = ops.uri

    def default_header(self):
        return {
            'API-key': self.key
        }

    def api_get(self,path: str,params: list,headers: dict) -> dict:
        headers.update(self.default_header())

        response = requests.get(
            self.uri + "3.0/" + path,
            headers=headers,
            params=params
        )
        return response.json()

    def api_get_walk(self,path: str,params: list,headers: dict,page:int=1):
        while page != None:
            data = self.api_get(path, params + [('page', page)], headers)
            page = data.get('pagination', {}).get('next_page', None)
            yield data

    def open_data_test(self,name:str):
        return json.load(open('./data_test/%s.json' % (name),'r'))


class ApiAffiseStats(ApiAffise):

    def __init__(self,date_from: date,date_to: date,limit_by_page: int):
        ApiAffise.__init__(self)
        self.date_from = date_from
        self.date_to = date_to
        self.limit_by_page = limit_by_page

    def params(self):
        return [
            ('filter[date_from]', str(self.date_from)),
            ('filter[date_to]', str(self.date_to)),
            ('limit', str(self.limit_by_page))
        ]

    def api_get_offer_walk(self):
        if ops.isEnvTest():
            dump = self.open_data_test('offer_stats')
            yield ModelAffiseStats.loads(dump,self.date_from)
            return
        for data in self.api_get_walk('stats/getbyprogram',self.params(),{}):
            yield ModelAffiseStats.loads(data,self.date_from)

class ApiAffiseOffer(ApiAffise):

    def __init__(self,limit_by_page: int):
        ApiAffise.__init__(self)
        self.limit_by_page = limit_by_page

    def params(self):
        return [
            ('limit',self.limit_by_page)
        ]

    def api_get_offers(self):
        data = self.api_get('offers',self.params(),{})
        #TODO ...
