from datetime import timedelta

class ScheduleDate:

    @staticmethod
    def schedule_by_delta(start, end, delta):
        curr = start
        while curr < end:
            yield curr
            curr += delta

    @staticmethod
    def schedule_by_day(start, end):
        return ScheduleDate.schedule_by_delta(
                    start,
                    end,
                    timedelta(days=1)
            )