from psycopg2 import connect
from typing import List
from ops import pg_dsn
from datetime import date
from collections import OrderedDict

class Storage:

    def columns(self,prefix=None) -> tuple:
        return tuple(self.values(prefix).keys())

    def values(self,prefix=None) -> OrderedDict:
        return OrderedDict(sorted({'%s_%s' % (prefix,key) if prefix != None else \
                    key:value for key,value in vars(self).items()}.items(),key= lambda t: t[0]))

    def update_excluded_columns(self) -> tuple: ...

# Save data with upsert query to postgresql
def save(s: List[Storage]):
    nb = len(s)
    if nb <= 0:
        print("No data")
        return
    baseData = s[0]

    # Construct QUERY
    columns = baseData.columns()
    sqlColumns = '('+','.join(columns)+')'

    sqlValues = []
    dataValues = []
    for i in range(nb):
        data = s[i]
        sqlValues.append('('+','.join("%%(%s)s" % \
                                      (column) for column in data.columns(i))+')')
        dataValues.append(data.values(i))

    sqlConfilct = '(' +','.join(baseData.update_excluded_columns()) + ')'
    sqlUpsert = ','.join("%(column)s = EXCLUDED.%(column)s" % \
                          {'column':column} \
                          for column in \
                            filter(lambda column: column not in data.update_excluded_columns(), columns))
    sql = """
INSERT INTO import_stats_offer %(sqlColumns)s
VALUES
  %(sqlValues)s
ON CONFLICT %(sqlConfilct)s DO UPDATE
  SET %(sqlUpsert)s
    """ % {
        'sqlColumns':sqlColumns,
        'sqlValues':',\n'.join(sqlValues),
        'sqlConfilct':sqlConfilct,
        'sqlUpsert':sqlUpsert
    }

    conn = connect(dsn=pg_dsn)
    curs = conn.cursor()
    curs.execute(sql,{key:value for items in dataValues for key,value in items.items()})
    conn.commit()
    conn.close()

class Model:

    @staticmethod
    def new(data:dict,date:date):
        raise NotImplementedError("New of model must be override")

    @classmethod
    def loads(cls,dump: dict,date:date) -> ...:
        stats = dump.get('stats')
        r = []
        for stat in stats:
            r.append(cls.new(stat, date))
        return r

class ModelAffiseStats(Storage,Model):

    def update_excluded_columns(self) -> tuple:
        return ('id','date')

    @staticmethod
    def new(data:dict,date:date):
        return ModelAffiseStats(data,date)

    def __init__(self,data:dict,date:date):
        self.date = date
        self.__init_slice_offer(data.get('slice',{}))
        self.__init_traffic(data.get('traffic',{}))
        self.__init_actions(data.get('actions',{}))
        self.views = data.get('views',None)
        self.ctr = data.get('ctr',None)
        self.__init_cr(data.get('cr',{}))
        self.ratio = data.get('ratio',None)
        self.epc = data.get('epc',None)

        Storage.__init__(self)
        Model.__init__(self)

    def __init_slice_offer(self,slice_offer:dict):
        offer = slice_offer.get('offer',{})
        self.id = offer.get('id',None)
        self.title = offer.get('title',None)
        self.offer_id = offer.get('offer_id',None)

    def __init_traffic(self,traffic:dict):
        self.traffic_row = traffic.get('raw')
        self.traffic_uniq = traffic.get('uniq')

    def __init_actions(self,actions:dict):
        for name,content in actions.items():
            setattr(self,'action_%s_charge' % (name),content.get('charge',None))
            setattr(self,'action_%s_earning' % (name),content.get('earning',None))
            setattr(self,'action_%s_revenue' % (name), content.get('revenue',None))
            setattr(self,'action_%s_null' % (name),content.get('null',None))
            setattr(self,'action_%s_count' % (name), content.get('count',None))

    def __init_cr(self,cr:dict):
        self.cr_confirmed = cr.get('confirmed',None)
        self.cr_declined = cr.get('declined', None)
        self.cr_hold = cr.get('hold', None)
        self.cr_not_found = cr.get('not_found', None)
        self.cr_pending = cr.get('pending', None)
        self.cr_pending_cap = cr.get('pending_cap', None)
        self.cr_total = cr.get('total',None)